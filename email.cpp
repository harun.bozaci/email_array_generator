#include <fstream>
#include <iostream>
#include <string>
#include <set>
#include <algorithm>
#include <chrono>

static const std::set<char> x{ ' ', '\n', '\r', '\t' };

std::string ltrim(std::string str)
{
	if(!str.empty()) {
		auto starts = std::find_if(str.begin(), str.end(), [](auto chr) {
			return x.find(chr) == x.end();
		});
		auto pos{std::distance(str.begin(), starts)};
		str = str.substr(pos, str.length());
	}
	return str;
}

std::string rtrim(std::string str)
{
	if(!str.empty()) {
		auto ends = std::find_if(str.rbegin(), str.rend(), [](auto chr) {
			return x.find(chr) == x.end();
		});
		auto pos{std::distance(str.rbegin(), ends)};
		str = str.substr(0, str.length() - pos);
	}
	return str;
}


void generate_html(std::string output_name, std::string output)
{
	std::ofstream outf{output_name + ".hpp"};

	outf << "#pragma once\n";
	outf << "#include <array>\n\n";
	outf << "static constexpr std::array<char," << output.length() << "> " << output_name << "{ ";

	std::size_t line_idx{};
	static const std::set<char> esc{'\\', '\''};

	for(auto ch : output) {

		if(line_idx > 0 && (line_idx % 15 == 0)) {
			outf << "\n\t\t\t\t\t\t";
		}

		std::string ToBePlaced{ch};

		if(esc.find(ch) != esc.end()) {
			ToBePlaced.insert(0, "\\");
		}

		outf << "'" << ToBePlaced << "'" << (line_idx != output.length() - 1 ? ',' : '}');
		line_idx++;
	}

	outf << ";\n";
	outf.close();
}

std::string consume_input(std::string input)
{
	std::ifstream input_html{input, std::ios::binary};
	if(!input_html)
	{
		std::cerr << "No such file " << input << '\n';
		std::exit(EXIT_FAILURE);
	}

	std::string email{};
	
	for(std::string line{}; std::getline(input_html, line, '\n'); ) {
		if(!line.empty()) {
			if(std::string trimmed{ rtrim(ltrim(std::move(line))) }; !trimmed.empty()) {
				email += trimmed;
			}
		}
	}

	input_html.close();
	return email;
}

int main(int argc, char **argv)
{
	if(argc != 3)
	{
		std::cerr << "usage:\n\t./email <input_html> <output_name>\n";
		std::exit(EXIT_FAILURE);
	}
	using tp = std::chrono::time_point<std::chrono::steady_clock>;
	tp start = std::chrono::steady_clock::now();
	std::string input_html(argv[1]);
	std::string output_name(argv[2]);

	generate_html(std::move(output_name), consume_input(std::move(input_html)));
	tp end = std::chrono::steady_clock::now();
	std::size_t diff = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
	std::cout << "Took " << diff << " us\n";
	return 0;
}
