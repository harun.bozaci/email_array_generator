#include <iostream>
#include "transaction_email.hpp"
#include "big_email.hpp"
#include <string>
#include <fstream>

int main(void)
{
	std::string output(big_email.begin(), big_email.end());
	std::ofstream email_trimmed{"trimmed.html"};
	email_trimmed.write(output.c_str(), output.size());

	return 0;
}
